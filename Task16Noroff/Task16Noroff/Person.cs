﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task16Noroff {
    class Person {
        #region variables/fields

        private string firstName;
        private string lastName;
        private string phoneNumber;
        private string email;

        #endregion

        #region constructors
        public Person() {

        }

        public Person(string firstName, string lastName, string phoneNumber, string email) {
            FirstName = firstName;
            LastName = lastName;
            PhoneNumber = phoneNumber;
            Email = email;
        }
        #endregion

        #region behavior

        // Method which returns the full name
        public string GetFullName() {
            return $"{firstName} {lastName}";
        }

        #endregion

        #region getter and setter
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string PhoneNumber { get => phoneNumber; set => phoneNumber = value; }
        public string Email { get => email; set => email = value; }
        #endregion

    }
}
