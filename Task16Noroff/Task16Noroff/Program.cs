﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task16Noroff {
    class Program {
        static void Main(string[] args) {
            
            Person[] peopleArray = new Person[3] {
            new Person("Per", "Olsen", "12345678", "per@mail.com"),
            new Person("Ole", "Hansen", "92345678", "ole@mail.com"),
            new Person("David", "Davidsen", "12345678", "david@mail.com"),
         };

            People peopleList = new People(peopleArray);
            // Prints out all names
            foreach (Person p in peopleList) {
                Console.WriteLine($"{p.FirstName} {p.LastName}");
            }

            // Create list and find person with firstname = Per and select getFullName using Linq
            IEnumerable<string> namesOfPeople =
               from Person person in peopleList
               where person.FirstName == "Per"
               select person.GetFullName();

            // Loop through list namesOfPeople and print out selected name
            foreach (var name in namesOfPeople) {
                Console.WriteLine("\nThe selected name using Linq query:");
                Console.WriteLine($"{name}");
            }

        }
    }
}

